package entity;

import java.io.Serializable;

   
//JPA (Begin)
import javax.persistence.*;
//JPA (End)   
  
 
@Entity
@Table(name = "\"Students\"")
public class Student implements Serializable
{
  @Id
  @Column(name = "\"Student_ID\"")
  private Integer studentID;
  @Column(name = "\"Student_Name\"")
  private String studentName;
  @Column(name = "\"Student_Add\"")
  private Integer studentAdd;
  
  public Integer getStudentID() {
    return studentID;
  }
  
  public void setStudentID(Integer sID) {
    studentID = sID;
  }  
  
  public String getStudentName() {
    return studentName;
  }
  
  public void setStudentName(String sName) {
    studentName = sName;
  }
  
  public Integer getStudentAdd() {
    return studentAdd;
  }
  
  public void setStudentAdd(Integer sAdd) {
    studentAdd = sAdd;
  }
}
