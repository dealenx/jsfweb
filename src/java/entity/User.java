/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author dealenx
 */
@Entity
@Table(name = "\"users\"")
public class User implements Serializable
{
  @Id
  @Column(name = "\"id\"")
  private Integer id;
  
  @Column(name = "\"name\"")
  private String name;
  
  @Column(name = "\"password\"")
  private String password;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

 

    
    
  
}
