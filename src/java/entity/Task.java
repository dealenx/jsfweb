/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author dealenx
 *
 *
 */

@Entity
@Table(name = "\"tasks\"")

public class Task {

    @Id
    @Column(name = "\"guid\"")
    private String guid;

    @Column(name = "\"userid\"")
    private int userid;

    @Column(name = "\"resdata\"")
    private String resdata;
    
    @Column(name = "\"typecalc\"")
    private String typecalc;

    @Column(name = "\"statusready\"")
    private Boolean statusready;

    @Column(name = "\"calcing_status\"")
    private Boolean calcing_status;

    @Column(name = "\"args\"")
    private String args;

    @Column(name = "\"name\"")
    private String name;

    public String getGuid() {
        return guid;
    }

    public int getUserid() {
        return userid;
    }

    public String getResdata() {
        return resdata;
    }

    public String getTypecalc() {
        return typecalc;
    }
    
    

    public Boolean getStatusready() {
        return statusready;
    }

    public Boolean getCalcing_status() {
        return calcing_status;
    }

    public String getArgs() {
        return args;
    }

    public String getName() {
        return name;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public void setResdata(String resdata) {
        this.resdata = resdata;
    }

    public void setStatusready(Boolean statusready) {
        this.statusready = statusready;
    }

    public void setCalcing_status(Boolean calcing_status) {
        this.calcing_status = calcing_status;
    }

    public void setArgs(String args) {
        this.args = args;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTypecalc(String typecalc) {
        this.typecalc = typecalc;
    }
    
    

}
