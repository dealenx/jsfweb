/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import entity.Student;
import java.util.Arrays;
import java.util.List;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

/**
 *
 * @author dealenx
 */
public class StudentEntityHelper {

    private EntityManagerFactory emf;

    @PersistenceContext
    private EntityManager em; 

    private UserTransaction utx;

    public StudentEntityHelper(EntityManagerFactory emf_p) {
        System.out.println("StudentEntityHelper(EntityManagerFactory emf)");
        try {
            emf_p = Persistence.createEntityManagerFactory("jsfwebPU");
            this.emf = emf_p;
            this.emf  = Persistence.createEntityManagerFactory("jsfwebPU");
            this.utx = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public Student getStudent(int id) {
        Student student = new Student();
        try {

            em = emf.createEntityManager();
          
            //Получение объекта 
            student = em.find(Student.class, id);

            //System.out.println("student.getStudentAdd(): " + student.getStudentAdd());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            em.close();
        }
        return student;
    }

    public void insertStudent(Student student) {

        try {
            em = emf.createEntityManager();
            utx.begin();
            em.joinTransaction();
            em.persist(student);
            utx.commit();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            em.close();
        }

    }

    // Если возникает ошибка, проверить данные с пустыми полями!
    public List<Student> getAll() {
        List<Student> students = Arrays.asList(new Student());
        try {
            em = emf.createEntityManager();
            //Query query = em.createQuery("Select s from Student s");
            students = (List<Student>)em.createQuery("Select e from Student e").getResultList(); 
            Student studentTest = students.get(0);
        
            System.out.println("student.getStudentAdd(): " + studentTest.getStudentAdd());
            //students = em.createQuery("SELECT s FROM Student s", Student.class).getResultList();
            

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            em.close();
        }
        //List<Student> students = em.createQuery("SELECT t FROM Person t", Student.class).getResultList();
        
        return students;
    }
/*
    public List<Person> findAllPersons(String Id) {
       
        em = emf.createEntityManager();
        em.getTransaction().begin();
        List<Person> listPersons = entityManager.createQuery(
                "SELECT p FROM Person p").getResultList();
        entityManager.getTransaction().commit();
        entityManager.close();
        factory.close();
        if (listPersons == null) {
            System.out.println("No persons found . ");
        } else {
            for (Person person : listPersons) {
                System.out.print("Person name= " + person.getName()
                        + ", gender" + person.getGender() + ", birthday="
                        + person.getBirthday());
            }
        }

        return listPersons;
    }*/

}
