/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import entity.Task;
import entity.User;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

/**
 *
 * @author dealenx
 */
public class TaskEntityHelper {
    

    private EntityManagerFactory emf;

    private EntityManager em;

    @Resource
    private UserTransaction utx;
    

    public TaskEntityHelper(EntityManagerFactory emf_p) { 
        
        try {
            emf_p = Persistence.createEntityManagerFactory("jsfwebPU");
            this.emf = emf_p;
            this.utx = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public Task getTask(String guid) {
        Task task = new Task();
        try {

            em = emf.createEntityManager();
          
            //Получение объекта 
            task = em.find(Task.class, guid);
            
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            em.close();
        }
        return task;
    }
    
    public void deleteTask(String guid) {
 
        try {
            
            em = emf.createEntityManager();
            utx.begin();
            em.joinTransaction();
            Task task = em.find(Task.class, guid);
            em.remove(task);
            utx.commit();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            em.close();
        }
 
    }
    
    public void deleteDataCalcRow(String guid) {
        deleteTask(guid);
    }
    
    public List<Task> getAll() {
        List<Task> tasks = Arrays.asList(new Task());
        try {
            em = emf.createEntityManager();
            
            tasks = (List<Task>)em.createQuery("Select t from Task t").getResultList(); 

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            em.close();
        }
 
        return tasks;
    }
    
    public List<Task> getAll(int id) {
        List<Task> tasks = Arrays.asList(new Task());
        try {
            em = emf.createEntityManager();
            
            
            Query query = em.createQuery("Select t from Task t WHERE t.userid = :custUserId")
                    .setParameter("custUserId", id);
            //Чтобы не кешировало
            query.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
            tasks = (List<Task>)query
                    .getResultList(); 
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            em.close();
        }
 
        return tasks;
    }  
    
    public ArrayList<String[]> getData(int user_id) {
        ArrayList<String[]> list = new ArrayList<String[]>();
        try {
            List<Task> tasks =  getAll(user_id);
            int i = 0;
            while (i < tasks.size() ) {

                String guid = tasks.get(i).getGuid();
                String name = tasks.get(i).getName();
                String typecalc = tasks.get(i).getTypecalc();
                String userid =  String.valueOf( tasks.get(i).getUserid() );
                Boolean statusready = tasks.get(i).getStatusready();
                Boolean calcing_status = tasks.get(i).getCalcing_status();
                String args = tasks.get(i).getArgs();
                String status;
                if( (statusready)&&(calcing_status) ) {
                    status = "Готово";
                } else if ( (statusready = false)&&(calcing_status == true) ) {
                    status = "В процессе";
                } else {
                    status = "Ожидание";
                }
                

                String[] ls = {
                    guid,
                    name,
                    typecalc,
                    userid,
                    String.valueOf(statusready),
                    String.valueOf(calcing_status),
                    args,
                    status
                };
                list.add(ls);
                i++;
            }
        } catch (Exception e) {
            System.out.println("getData = " + e);
        }

        return list;
    }
    
    public String getDataResult(int user_id, String guid) {
        String resdata = "";
        Task task = new Task();
        try {

            em = emf.createEntityManager();
          
            //Получение объекта 
            task = em.find(Task.class, guid);
            resdata = task.getResdata();
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            em.close();
        }
        
        return resdata;
    }
    
    public void insertTask(Task task) {

        try {
            em = emf.createEntityManager();
            utx.begin();
            em.joinTransaction();
            em.persist(task);
            em.flush();
            utx.commit();
            

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            em.close();
        }

    }
    
    public void sendTaskSimpleAB(int UserID, String type_calc, String args) {
        try {
            UUID uuid = UUID.randomUUID();
            String randomUUIDString = uuid.toString();
            String name = "simpleNAME";
            int user_id = UserID;
            String res = "";
            boolean statusready = false;
            boolean calcing_status = false;
            
            Task task = new Task();
         
                    
            task.setGuid( randomUUIDString );
            task.setUserid(user_id);
            task.setStatusready(statusready);
            task.setCalcing_status(calcing_status);
            task.setArgs(args);
            task.setName(name);
            task.setTypecalc(type_calc);

            insertTask(task);
                    
        } catch (Exception e) {
            System.out.println("Error write data base: " + e);
        }
    }
    
    public void sendTask(int UserID, String name, String type_calc, String args) {
        try {
            UUID uuid = UUID.randomUUID();
            String randomUUIDString = uuid.toString();
            int user_id = UserID;
            String res = "";
            boolean statusready = false;
            boolean calcing_status = false;
            
            Task task = new Task();
         
                    
            task.setGuid( randomUUIDString );
            task.setUserid(user_id);
            task.setStatusready(statusready);
            task.setCalcing_status(calcing_status);
            task.setArgs(args);
            task.setName(name);
            task.setTypecalc(type_calc);

            insertTask(task);
                    
        } catch (Exception e) {
            System.out.println("Error write data base: " + e);
        }
    }
    
}