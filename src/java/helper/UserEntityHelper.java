/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import entity.User;
import java.util.Arrays;
import java.util.List;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.UserTransaction;


/**
 *
 * @author dealenx
 */
public class UserEntityHelper {

    private EntityManagerFactory emf;

    private EntityManager em;

    private UserTransaction utx;

    public UserEntityHelper(EntityManagerFactory emf_p) {
        
        try {
            emf_p = Persistence.createEntityManagerFactory("jsfwebPU");
            this.emf = emf_p;
            this.utx = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    //List<YourObject> list = criteria.add(Restrictions.eq("yourField", yourFieldValue)).list();
    public User getUser(int id) {
        User user = new User();
        try {

            em = emf.createEntityManager();
          
            //Получение объекта 
            user = em.find(User.class, id);

            //System.out.println("student.getStudentAdd(): " + student.getStudentAdd());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            em.close();
        }
        return user;
    }
    
    public String getPasswordByName(String name) {
        String password = "";
        try {
            em = emf.createEntityManager();
            
            List<User> users = (List<User>)em
                    .createQuery("Select u from User u WHERE u.name LIKE :custName")
                    .setParameter("custName", name)
                    .setMaxResults(1).getResultList(); 
            password = users.get(0).getPassword();
            //User studentTest = users.get(0);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            em.close();
        }
        //List<Student> students = em.createQuery("SELECT t FROM Person t", Student.class).getResultList();
        
        return password;
    }
    
    public int getUserId(String name) {
        int userid = 0;
        try {
            em = emf.createEntityManager();
            
            List<User> users = (List<User>)em
                    .createQuery("Select u from User u WHERE u.name LIKE :custName")
                    .setParameter("custName", name)
                    .setMaxResults(1).getResultList(); 
            userid = users.get(0).getId();
 

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            em.close();
        }

        return userid;
    }
    
    public boolean isAllowed(String username, String password) {
        String pass_temp = getPasswordByName(username);
        System.out.println("pass from bd: " + pass_temp);
        if(password.equals(pass_temp)) {
            return true;
        } else {
            return false;
        }
    }

    public void insertStudent(User user) {

        try {
            em = emf.createEntityManager();
            utx.begin();
            em.joinTransaction();
            em.persist(user);
            utx.commit();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            em.close();
        }

    }

    // Если возникает ошибка, проверить данные с пустыми полями!
    public List<User> getAll() {
        List<User> users = Arrays.asList(new User());
        try {
            em = emf.createEntityManager();
            
            users = (List<User>)em.createQuery("Select u from User u").getResultList(); 
            //User studentTest = users.get(0);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            em.close();
        }
        //List<Student> students = em.createQuery("SELECT t FROM Person t", Student.class).getResultList();
        
        return users;
    }


}
