/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.UUID;
import javax.annotation.Resource;
import javax.naming.InitialContext;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;

/**
 *
 * @author dealenx
 */
public class DataSourceConnect {

    private String url, login, password;
    private String nameSource;
    private Connection con;
    private Statement statement;
    private ResultSet rs;

    public boolean errorDataBase;
    public String infoError;
    private int countRows;

    //@Resource(name = "jdbc/pgsql")
    private DataSource ds;


    public DataSourceConnect() {
        System.out.println("DataBaseConnect()");
        init();
        connect();
    }

    private void init() {
        this.nameSource = "jdbc/localpg";
    }

    public void connect() {

        try {
            InitialContext initialContext = new InitialContext();
            ds = (DataSource) initialContext.lookup(this.nameSource);
        } catch (Exception e) {
             System.out.println("JNDI: " + e.getMessage());
        }

        try {
            this.con = ds.getConnection();
            try {
                this.statement = this.con.createStatement();
            } catch (Exception e) {
                System.out.println("Process error: " + e.getMessage());
            }

        } catch (Exception e) {
            System.out.println("Connection error: " + e.getMessage());
        }		  
    }

    public void disConnect() {
        System.out.println("disConnect()");
        try {
            rs.close();

            try {
                statement.close();

                try {
                    con.close();
                } catch (Exception e) {
                    errorDataBase = true;
                    infoError = "Error disconnect data base: " + e;
                }

            } catch (Exception e) {
                errorDataBase = true;
                infoError = "Error statement close: " + e;
            }

        } catch (Exception e) {
            errorDataBase = true;
            infoError = "Error close tables: " + e;
        }
    }

    public String getPassword(String name) {
        String temp = "";
        try {
            String sql = "SELECT password FROM \"users\" WHERE name = \'" + name + "\';  ";
            this.rs = this.statement.executeQuery(sql);
            this.rs.next();
            temp = this.rs.getString("password");

        } catch (Exception e) {
            System.out.println("getData = " + e);
        }

        System.out.println("tmp: " + temp);
        return temp;
    }

    public ArrayList<String[]> getData(int user_id) {
        ArrayList<String[]> list = new ArrayList<String[]>();
        try {
            String sql = "SELECT * FROM  \"tasks\"  WHERE userid = '" + user_id + "' ;";
            this.rs = this.statement.executeQuery(sql);
            while (this.rs.next()) {
                String guid = this.rs.getString("guid");
                String name = this.rs.getString("name");
                String typecalc = this.rs.getString("typecalc");
                String userid = this.rs.getString("userid");
                Boolean statusready = this.rs.getBoolean("statusready");
                Boolean calcing_status = this.rs.getBoolean("calcing_status");
                String args = this.rs.getString("args");

                String[] ls = {
                    guid,
                    name,
                    typecalc,
                    userid,
                    String.valueOf(statusready),
                    String.valueOf(calcing_status),
                    args};
                list.add(ls);
                System.out.println("ls[0]" + ls[0]);
            }
        } catch (Exception e) {
            System.out.println("getData = " + e);
        }

        return list;
    }

    public String getDataResult(int user_id, String guid) {
        String resdata = "";
        try {
            String sql = "SELECT resdata FROM  \"tasks\"  WHERE userid = '" + user_id + "' and guid = '" + guid + "' ;";
            this.rs = this.statement.executeQuery(sql);
            while (this.rs.next()) {
                resdata = this.rs.getString("resdata");
            }
        } catch (Exception e) {
            System.out.println("getDataRes = " + e);
        }

        return resdata;
    }

    public void deleteDataCalcRow(String guid) {
        try {
            String sql = "DELETE FROM \"tasks\" "
                    + "WHERE guid = '" + guid + "' ;";
            this.statement.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println("getData = " + e);
        }
    }

    public int getUserId(String name) {
        int tmpuserid = 0;
        try {
            String sql = "SELECT id FROM  \"users\"  WHERE name = '" + name + "' ;";
            this.rs = this.statement.executeQuery(sql);
            while (this.rs.next()) {
                tmpuserid = this.rs.getInt("id");
            }
        } catch (Exception e) {
            System.out.println("getData = " + e);
        }

        return tmpuserid;
    }

    public void sendTaskSimpleAB(int UserID, String type_calc, String args) {
        try {
            UUID uuid = UUID.randomUUID();
            String randomUUIDString = uuid.toString();
            String name = "simpleNAME";
            int user_id = UserID;
            String res = "";
            boolean statusready = false;
            boolean calcing_status = false;
            String data = "INSERT INTO \"tasks\" ( guid, name, typecalc, userid, resdata, statusready, args, calcing_status) "
                    + "VALUES ( \'" + randomUUIDString  + "\', \'" + name + "\', \'" + type_calc + "\', " + user_id + ", \'" + res + "\', " + statusready + ", \'" + args+ "\'" + "," + calcing_status + "" + " );";

            this.statement.executeUpdate(data);
        } catch (Exception e) {
            System.out.println("Error write data base: " + e);
        }
    }

}
