/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import entity.Student;
import helper.StudentEntityHelper;
import helper.UserEntityHelper;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;

import model.*;

@Named("login")
@SessionScoped
public class LoginBean implements Serializable {

    @PersistenceUnit
    private EntityManagerFactory emf;

    private UserEntityHelper userHelper;


    public LoginBean() {
        FacesContext fCtx = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
        String sessionId = session.getId();
        System.out.println("LoginBean session id: " + sessionId);
        //model = new Model();

        
        userHelper = new UserEntityHelper(emf);
    }

    private String name;
    private String password;

    public String getName() {
        return this.name;
    }
/*
    public Model getModel() {
        return this.model;
    }*/

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTest() {
        return "TESSSSSST!";
    }

    public String doLogin() {
        String navResult = "";
        System.out.println("Entered Username is= " + this.name + ", password is= " + this.password);
        if (this.userHelper.isAllowed(this.name, this.password)) {
            navResult = "success";
        } else {
            navResult = "failure";
        }
        return navResult;
    }

    public void doLogout() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().invalidateSession();
        String contextPath = FacesContext.getCurrentInstance().getExternalContext().getContextName();
        System.out.println("contextPath");
        System.out.println(contextPath);
        //validateUserLogin(); 
        try {
            //this.db.disConnect();
            context.getExternalContext().redirect("/jsfweb/");

            //origRequest.sendRedirect("http://" + origRequest.getServerName() + ":" + origRequest.getServerPort() + origRequest.getContextPath() + "/");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
