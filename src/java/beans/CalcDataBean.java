/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import entity.Task;
import helper.TaskEntityHelper;
import helper.UserEntityHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import model.*;

@Named("calc")
@SessionScoped
public class CalcDataBean implements Serializable {

    @PersistenceUnit
    private EntityManagerFactory emf;

    private UserEntityHelper userHelper;

    //private static  Model model;
    private TaskEntityHelper taskHelper;

    private String testAlert;
    private String typeCalc;
    

    private List<String[]> data;
    
    private String inputHelmgoltzEps = "";
    private String inputHelmgoltzNameFile = "";
    private String inputHelmgoltzK = "";
    private String inputHelmgoltzHoles = ""; 
    
    

    private String inputTransportSubNameFile = "";
    private String inputTransportSubD = "";
    private String inputTransportSubM = "";
    private String inputTransportSubEndTime = "";
    
 
    private ArrayList<Map<String, String>> inputTask1;
    private Map<String, String> inputTask1Map;
    

    List<Task> tasks;

    //private Map<String, String> inputTask1 = new HashMap<String, String>();
    private String helmgoltz_choice;
    private Map<String, String> helmgoltz_choices = new HashMap<String, String>();

    public ArrayList<Map<String, String>>  getinputTask1() {
        return inputTask1;
    }

    public void setFavorites(ArrayList<Map<String, String>>  inputTask1) {
        this.inputTask1 = inputTask1;
    }

    public String getHelmgoltz_choice() {
        return helmgoltz_choice;
    }

    public void setHelmgoltz_choice(String helmgoltz_choice) {
        this.helmgoltz_choice = helmgoltz_choice;
    }

    public Map<String, String> getHelmgoltz_choices() {
        return helmgoltz_choices;
    }

    public void setHelmgoltz_choices(Map<String, String> helmgoltz_choices) {
        this.helmgoltz_choices = helmgoltz_choices;
    }

    
    
   
  
    public String getInputHelmgoltzEps() {
        return inputHelmgoltzEps;
    }

    public void setInputHelmgoltzEps(String inputHelmgoltzEps) {
        this.inputHelmgoltzEps = inputHelmgoltzEps;
    }

    public String getInputHelmgoltzNameFile() {
        return inputHelmgoltzNameFile;
    }

    public void setInputHelmgoltzNameFile(String inputHelmgoltzNameFile) {
        this.inputHelmgoltzNameFile = inputHelmgoltzNameFile;
    }

    public String getInputHelmgoltzK() {
        return inputHelmgoltzK;
    }

    public void setInputHelmgoltzK(String inputHelmgoltzK) {
        this.inputHelmgoltzK = inputHelmgoltzK;
    }

    public String getInputHelmgoltzHoles() {
        return inputHelmgoltzHoles;
    }

    public void setInputHelmgoltzHoles(String inputHelmgoltzHoles) {
        this.inputHelmgoltzHoles = inputHelmgoltzHoles;
    }

    public String getInputTransportSubNameFile() {
        return inputTransportSubNameFile;
    }

    public void setInputTransportSubNameFile(String inputTransportSubNameFile) {
        this.inputTransportSubNameFile = inputTransportSubNameFile;
    }

    public String getInputTransportSubD() {
        return inputTransportSubD;
    }

    public void setInputTransportSubD(String inputTransportSubD) {
        this.inputTransportSubD = inputTransportSubD;
    }

    public String getInputTransportSubM() {
        return inputTransportSubM;
    }

    public void setInputTransportSubM(String inputTransportSubM) {
        this.inputTransportSubM = inputTransportSubM;
    }

    public String getInputTransportSubEndTime() {
        return inputTransportSubEndTime;
    }

    public void setInputTransportSubEndTime(String inputTransportSubEndTime) {
        this.inputTransportSubEndTime = inputTransportSubEndTime;
    }
    
    

    @Inject
    UserBean user;

    @PostConstruct
    public void initBeans() {
        //this.model = user.getModel();
        update();

    }

    public CalcDataBean() {
        System.out.println("CalcDataBean() ");
        
        this.data = new ArrayList<String[]>();
        this.testAlert = "0";
        this.typeCalc = "any";

        userHelper = new UserEntityHelper(this.emf);
        taskHelper = new TaskEntityHelper(this.emf);

    }
    
    public void addInputTask1() {
        inputTask1.add(inputTask1Map);
    }

    public void update() {
        
        tasks = this.taskHelper.getAll(user.getUserid());
        List<String[]> tempList = geTableFromTasks(tasks);
        setData(tempList);

        
        helmgoltz_choices = new HashMap<String, String>();
        helmgoltz_choices.clear();

 
        for(int i = 0; i < tasks.size(); i++) {  
            if( (tasks.get(i).getTypecalc().equals("helmholtz")) && tasks.get(i).getStatusready() ) {
                helmgoltz_choices.put(tasks.get(i).getName(), tasks.get(i).getGuid());
            }
            
        }
 
    }
    
    
    

    public ArrayList<String[]> geTableFromTasks(List<Task> tasks_p) {
        ArrayList<String[]> list = new ArrayList<String[]>();
        try {
            List<Task> tasks_temp = tasks_p;
            int i = 0;
            while (i < tasks.size()) {

                Boolean statusready = tasks.get(i).getStatusready();
                Boolean calcing_status = tasks.get(i).getCalcing_status();
                String args = tasks.get(i).getArgs();
                String status;
                if ((statusready) && (calcing_status)) {
                    status = "Готово";
                } else if ((statusready = false) && (calcing_status == true)) {
                    status = "В процессе";
                } else {
                    status = "Ожидание";
                }

                String[] ls = {
                    tasks.get(i).getGuid(),
                    tasks.get(i).getName(),
                    tasks.get(i).getTypecalc(),
                    String.valueOf(tasks.get(i).getUserid()),
                    String.valueOf(statusready),
                    String.valueOf(calcing_status),
                    tasks.get(i).getArgs(),
                    status
                };
                list.add(ls);
                i++;
            }
        } catch (Exception e) {
            System.out.println("getData = " + e);
        }

        return list;
    }

    public void download(String guid, String name) throws IOException {

        String temp_get = this.taskHelper.getDataResult(user.getUserid(), guid);
        byte[] buffer_temp = temp_get.getBytes();

        FacesContext facesContext = FacesContext.getCurrentInstance();

        HttpServletResponse response
                = (HttpServletResponse) facesContext.getExternalContext().getResponse();

        response.reset();
        response.setHeader("Content-Type", "application/octet-stream");
        response.setHeader("Content-Disposition", "attachment;filename=" + name + ".txt");

        OutputStream outStream = response.getOutputStream();

        byte[] buffer = new byte[4096];
        int bytesRead = -1;

        outStream.write(buffer_temp);

        outStream.close();

        facesContext.responseComplete();

    }

    public String getTypeCalc() {
        return typeCalc;
    }

    public void setTypeCalc(String typeCalc) {
        this.typeCalc = typeCalc;
    }

    public String getTestAlert() {
        return testAlert;
    }

    public void setTestAlert(String testAlert) {
        this.testAlert = testAlert;
    }

    public void updateTestAlert(String test) {
        setTestAlert(test);
    }

    public void sendTask() {
        String args
                = "{"
                + "a: " + 1+ ","
                + "b: " + 6
                + "}";
        this.taskHelper.sendTaskSimpleAB(user.getUserid(), "simple", args);
    }
    
    public void sendTaskHelmholtz() {
        //JsonObject objectJson = new JsonObject();
        String strPoints =  "{\n" + 
        "  \"points\": [\n" +
        "    {\n" +
        "      \"number\": \"1\",\n" +
        "      \"y0\": \"0.4\",\n" +
        "      \"y1\": \"0.6\",\n" +
        "      \"u\": \"2\",\n" +
        "      \"flag\": \"0\"\n" +
        "    },\n" +
        "    {\n" +
        "      \"number\": \"3\",\n" +
        "      \"y0\": \"0.4\",\n" +
        "      \"y1\": \"0.6\",\n" +
        "      \"u\": \"2\",\n" +
        "      \"flag\": \"1\"\n" +
        "    }\n" +
        "  ]\n" +
        "}";
        Gson gson = new Gson();
        JsonElement jsonElement= gson.fromJson (inputHelmgoltzHoles, JsonElement.class);
        JsonObject jsonObj = jsonElement.getAsJsonObject();
        //jsonObj.addProperty("name", inputHelmgoltzNameFile);
        jsonObj.addProperty("k", inputHelmgoltzK);
        jsonObj.addProperty("eps", inputHelmgoltzEps);
        
        String args = jsonObj.toString();
        this.taskHelper.sendTask(user.getUserid(), inputHelmgoltzNameFile, "helmholtz", args);
    }
    
    public void sendTaskTransportSub() {
        //JsonObject objectJson = new JsonObject();
        String strSubstances =  "{\n" + 
        "  \"substances\": [\n" +
        "    {\n" +
        "      \"x1\": \"0.4\",\n" +
        "      \"y1\": \"0.4\",\n" +
        "      \"x2\": \"0.6\",\n" +
        "      \"y2\": \"0.6\"\n" +
        "    }\n" +
      
        "  ]\n" +
        "}";
        Gson gson = new Gson();
        JsonElement jsonElement= gson.fromJson (strSubstances, JsonElement.class);
        JsonObject jsonObj = jsonElement.getAsJsonObject();
   
        jsonObj.addProperty("m", inputTransportSubM );
        jsonObj.addProperty("d", inputTransportSubD );
        jsonObj.addProperty("endtime", inputTransportSubEndTime );
        jsonObj.addProperty("helmgoltz_guid", helmgoltz_choice);
        
        String args = jsonObj.toString();
        this.taskHelper.sendTask(user.getUserid(), inputTransportSubNameFile, "transportsub", args);
    }

    public void sendTaskABC() {
        String args
                = "{"
                + "a: " + 1 + ","
                + "b: " + 4 + ","
                + "c: " + 3
                + "}";
        this.taskHelper.sendTaskSimpleAB(user.getUserid(), "simpleABC", args);
    }

    public void deleteDataRow(String guid) {
        setTestAlert(guid);
        this.taskHelper.deleteDataCalcRow(guid);
    }

    public List<String[]> getFilterData() {
        ArrayList<String[]> tempData = new ArrayList<String[]>();
        if (typeCalc.equals("any")) {
            return data;
        } else {
            for (int i = 0; i < data.size(); i++) {
                if (typeCalc.equals(data.get(i)[2])) {
                    tempData.add(data.get(i));
                }
            }
            return tempData;
        }

    }

    public List<String[]> getData() {
        update();
        return getFilterData();
    }

    public void setData(List<String[]> data) {
        this.data = data;
    }
}
