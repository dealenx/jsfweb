/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import entity.Student;
import entity.User;
import helper.StudentEntityHelper;
import helper.UserEntityHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;
import javax.servlet.http.HttpSession;
import model.EntityController;
import model.Model;

@Named("user")
@SessionScoped
public class UserBean implements Serializable {

    @PersistenceUnit
    private EntityManagerFactory emf;

    //private static Model model;

    private int userid;
    private String name;
    private String password;
    private String sessionid;
    
    private UserEntityHelper userHelper;
    
    
    public EntityManagerFactory getEmf() {
        return emf;
    }

    public UserEntityHelper getUserHelper() {
        return userHelper;
    }
    
    

    public UserBean() {
        System.out.println("UserBean()");
        setName("");
        setPassword("");
        checkSessionId();
        
        
        userHelper = new UserEntityHelper(this.emf);

    }

    @Inject
    LoginBean login;

    @PostConstruct
    public void initBeans() {
        init(login.getName(), login.getPassword());
        //this.model = login.getModel();
    }

    public void init() {
        setName("admin");
        setPassword("123");
        setUserid(0);
    }

    public void init(String name, String password) {
        setName(name);
        setPassword(password);
        int tmpuserid = this.userHelper.getUserId( getName() );
        setUserid(tmpuserid);
    }
/*
    public Model getModel() {
        return model;
    }*/

    public void checkSessionId() {
        FacesContext fCtx = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
        String sessionId = session.getId();
        System.out.println("MainBean session id: " + sessionId);
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
