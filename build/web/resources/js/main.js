window.onload = function () {

    var inputs = [
        
    ];
    
    var test =  "{\n" + 
        "  \"points\": [\n" +
        "    {\n" +
        "      \"number\": \"1\",\n" +
        "      \"y0\": \"0.4\",\n" +
        "      \"y1\": \"0.6\",\n" +
        "      \"u\": \"2\",\n" +
        "      \"flag\": \"0\"\n" +
        "    },\n" +
        "    {\n" +
        "      \"number\": \"3\",\n" +
        "      \"y0\": \"0.4\",\n" +
        "      \"y1\": \"0.6\",\n" +
        "      \"u\": \"2\",\n" +
        "      \"flag\": \"1\"\n" +
        "    }\n" +
        "  ]\n" +
        "}";

    function refresh() {
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].number = $("div").find("[hole='" + i + "'] .number").val()
            inputs[i].y0 = $("div").find("[hole='" + i + "'] .y0").val()
            inputs[i].y1 = $("div").find("[hole='" + i + "'] .y1").val()
            inputs[i].u = $("div").find("[hole='" + i + "'] .u").val()
            inputs[i].flag = $("div").find("[hole='" + i + "'] .flag").val()
        }
        if (inputs.length == 0) {
            $("#jsonholes input").val("");
        } else {
           $("#jsonholes input").val(JSON.stringify({holes: inputs })); 
        }
        
        

    }
    $(document).on("keyup", function () {
        refresh()
    });

    $("#add").click(function () {
        $('<div id="input_hole_' + inputs.length + '" hole="' + inputs.length + '"></div>')
                .appendTo('#inputs');
        $('<p>Отвестие #' + inputs.length + '</p>')
                .appendTo('#input_hole_' + inputs.length + '');
        $('<input placeholder="number" name="text" class="number" required ><br>')
                .appendTo('#input_hole_' + inputs.length + '');
        $('<input placeholder="y0" name="text" class="y0" required >')
                .appendTo('#input_hole_' + inputs.length + '');
        $('<input placeholder="y1" name="text" class="y1" required > ')
                .appendTo('#input_hole_' + inputs.length + '');
        $('<input placeholder="u" name="text" class="u" required > ')
                .appendTo('#input_hole_' + inputs.length + '');
        $('<input placeholder="flag" name="text" class="flag" required > ')
                .appendTo('#input_hole_' + inputs.length + '');
        $('<hr />')
                .appendTo('#input_hole_' + inputs.length + '');
        
        inputs.push({number: "",y0: "", y1: "",u: "", flag: ""}); 
        refresh();
    });

}